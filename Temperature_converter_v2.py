#Imports
import easygui as gui



#1. Ask user which temperature want to convert

choice = gui.buttonbox('What would you like to convert?',
                       title = 'Temperature conerter',
                       choices = ("C to F", "F to C"))

if choice == "C to F":
    temp_cel = gui.enterbox("Enter a temperature in degrees C: ", title = 'Temperature converter')

    def convert_cel_to_far(temp_cel):
        '''Convert Celsius to Fahrenheit'''
        far = int(temp_cel) * (9/5) + 32
        return far

    gui.msgbox(f"{temp_cel} degrees C = {convert_cel_to_far(temp_cel):.2f} degrees F", title = 'Temperature converter')

else:

    temp_far = gui.enterbox("Enter a temperature in degrees F: ", title = 'Temperature converter')

    def convert_far_to_cel(temp_far):
        '''Convert Fahrenheit to Celsius'''
        cel = (int(temp_far) - 32) * (5/9)
        return cel

    gui.msgbox(f"{temp_far} degrees F = {convert_far_to_cel(temp_far):.2f} degrees C", title = 'Temperature converter')

